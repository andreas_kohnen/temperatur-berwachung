#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<Windows.h>

#define blue 1
#define green 2
#define red 4

#define d 288
#define e 3

//////////
//Struct//
//////////
struct data {
	float temp[e];
	time_t timestamp;
};

////////////
//Prototyp//
////////////
void werteerzeugen(int anzahl, data Speicher[]);
void ausgabe(int anzahl, data Speicher[]);


////////////
//Int Main//
////////////
int main()
{
	data Speicher[d];//Warum übernimmt er die Werte nicht aus der Klammer von struct data?//
	time_t now;
	now = time(NULL);

	werteerzeugen(d, Speicher);
	ausgabe(d, Speicher);


	return 0;
}

////////////
//Funktion//
////////////
void werteerzeugen(int anzahl, data Speicher[])
{
	time_t now;
	now = time(NULL);
	srand(time(NULL));
	for (int i = 0; i < d; i++) {
		for (int j = 0; j < e; j++) {
			float q = ((rand() % 86 + 200));

			Speicher[i].temp[j] = q / 10;
		}
		Speicher[i].timestamp = time(&now);
	}
}
void ausgabe(int anzahl, data Speicher[])
{
	char localtime[64];
	tm localtimetm;
	FILE* fp;//fp Stream zeigt auf Datei im System
	fopen_s(&fp, "datei.txt", "w");// weitere Dateien möglich? Wenn ja, wie? Stringfunktion?
	//Schrittweises Beschreiben möglich?
	SetConsoleTextAttribute(::GetStdHandle(STD_OUTPUT_HANDLE), green);
	fwrite(Speicher,sizeof (data),anzahl,fp);
	fclose(fp);
	fopen_s(&fp, "datei.xls", "w");
	for (int i = 0; i < anzahl; i++)
	{
		localtime_s(&localtimetm, &Speicher[i].timestamp);
		asctime_s(localtime, 64, &localtimetm);
		printf("\n%s %i.Wert: \t%.1f\t %.1f\t %.1f ", localtime, i + 1,
			Speicher[i].temp[0], Speicher[i].temp[1], Speicher[i].temp[2]);
		fprintf(fp, "\n%s  %i.Wert: \t%.1f\t %.1f\t %.1f ", localtime, i + 1,
			Speicher[i].temp[0], Speicher[i].temp[1], Speicher[i].temp[2]);//ganze Zahlen abspeichern zum späteren Einlesen der Datei
	}
	fclose(fp);
}
